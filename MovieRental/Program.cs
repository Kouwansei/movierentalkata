﻿using System;
using System.Collections.Generic;

namespace MovieRentalKata
{
    enum PriceCode
    {
        REGULAR,
        NEW_RELEASE,
        CHILDRENS
    }

    abstract class Price
    {
        public abstract PriceCode PriceCode { get; }
        public abstract double GetCharge(int DaysRented);

        public virtual int GetFrequentRenterPoints(int daysRented) 
        {
            return 1;
        }
    }

    class ChildrensPrice : Price
    {
        public override PriceCode PriceCode 
        {
            get => PriceCode.CHILDRENS;
        }

        public override double GetCharge(int DaysRented)
        {
           double result = 1.5;
            if (DaysRented > 3)
                result += (DaysRented - 3) * 1.5;

            return result;
        }
    }

    class NewReleasePrice : Price
    {
        public override PriceCode PriceCode
        {
            get => PriceCode.NEW_RELEASE;
        }
        public override double GetCharge(int DaysRented)
        {
            return DaysRented * 3;
        }

        public override int GetFrequentRenterPoints(int daysRented)
        {
            
            return (daysRented > 1) ? 2 : 1;
        }
    }

    class RegularPrice : Price
    {
        public override PriceCode PriceCode
        {
            get => PriceCode.REGULAR;
        }
        public override double GetCharge(int DaysRented)
        {
            double result = 2;
            if (DaysRented > 2)
                result += (DaysRented - 2) * 1.5;

            return result;
        }
    }

    class Movie
    {
        private String title;
        private Price price;

        public Movie(String title, PriceCode priceCode)
        {
            this.title = title;
            this.PriceCode = priceCode;
        }

        public PriceCode PriceCode
        {
            get =>  price.PriceCode;
            set 
            {
                //priceCode = value;
                switch (value) 
                {
                    case PriceCode.CHILDRENS:
                        price = new ChildrensPrice();
                        break;
                    case PriceCode.NEW_RELEASE:
                        price = new NewReleasePrice();
                        break;
                    case PriceCode.REGULAR:
                        price = new RegularPrice();
                        break;
                }
            }
        }

        public String Title
        {
            get => title;
        }

        public double GetCharge(int DaysRented)
        {
            return price.GetCharge(DaysRented);
        }

        public int GetFrequentRenterPoints(int DaysRented)
        { 
            return price.GetFrequentRenterPoints(DaysRented);
        }

    }

    class Rental
    {
        private Movie movie; 
        private int daysRented;

        public Rental(Movie movie, int daysRented)
        {
            this.movie = movie;
            this.daysRented = daysRented;
        }

        public int DaysRented
        {
            get => daysRented;
        }

        public Movie Movie
        {
            get => movie;
        }

        public double GetCharge()
        {
             return movie.GetCharge(DaysRented);
        }

        public int GetFrequentRenterPoints()
        {
            return movie.GetFrequentRenterPoints(DaysRented);
        }
    }

    class Customer
    {
        private String name;
        private List<Rental> rentals = new List<Rental>();

        public Customer(String name)
        {
            this.name = name;
        }

        public void AddRental(Rental rental)
        {
            rentals.Add(rental);
        }

        public String Name
        {
            get => name;
        }

        public String Statement()
        {

            String result = "Rental Record for " + Name + "\n";

            foreach (var rental in rentals)
            {

                // Show figures for this rental
                result += "\t" + rental.Movie.Title + "\t" + rental.GetCharge().ToString() + "\n";

            }

            // Add footer lines
            result += "Amount owed is " + GetTotalCharge().ToString() + "\n";
            result += "You earned " + GetTotalFrequentRenterPoints().ToString() + " frequent renter points";

            return result;
        }

        public String HTMLStatement()
        {

            String result = "<h1Rental Record for <em>" + Name + "</em><h1>\n";

            result += "</ol>\n";
            foreach (var rental in rentals)
            {
                // Show figures for this rental
                result += "<li><em>" + rental.Movie.Title + "</em: " + rental.GetCharge().ToString() + "</li>\n";
            }
            result += "</ol>\n";

            // Add footer lines
            result += "<p>";
            result += "Amount owed is <em>" + GetTotalCharge().ToString() + "</em>\n";
            result += "</p>";

            result += "<p>";
            result += "You earned <em>" + GetTotalFrequentRenterPoints().ToString() + "</em> frequent renter points";
            result += "</p>";
            return result;
        }

        public double GetTotalCharge()
        {
            double totalAmount = 0;

            foreach (var rental in rentals)
            {
                totalAmount += rental.GetCharge();
            }

            return totalAmount;
        }

        public int GetTotalFrequentRenterPoints()
        {
            int frequentRenterPoints = 0;

            foreach (var rental in rentals)
            {
                frequentRenterPoints += rental.GetFrequentRenterPoints();
            }

            return frequentRenterPoints;

        }


        class MainClass
        {
            public static void Main(string[] args)
            {
                var movie01 = new Movie("Captain Marvel", PriceCode.NEW_RELEASE);
                var movie02 = new Movie("Aladdin", PriceCode.CHILDRENS);
                var movie03 = new Movie("Back To The Future", PriceCode.REGULAR);

                var customer = new Customer("Roger Ebert");

                var rental01 = new Rental(movie01, 2);
                var rental02 = new Rental(movie02, 3);
                var rental03 = new Rental(movie03, 2);

                customer.AddRental(rental01);
                customer.AddRental(rental02);
                customer.AddRental(rental03);

                Console.WriteLine(customer.Statement());

            }
        }
    }
}
